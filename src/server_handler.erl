-module(server_handler).
-export([start/0,get_numbers/3,create_number/3, get_all_numbers/3,received_call/3, call/3,get_call/3,close_all/3]).
-import(server_table, [get_all_users/0,insert_number/2, get_user/1,update_number/2,update_number_call/2,get_call/1,close_call/1]).
-import(string,[substr/3]). 
-import(string,[concat/2]).
start() ->
    inets:start(),
    inets:start(httpd, [
        {modules, [
            mod_alias,
            mod_auth,
            mod_esi,
            mod_actions,
            mod_cgi,
            mod_dir,
            mod_get,
            mod_head,
            mod_log,
            mod_disk_log
        ]},
        {port, 8090},
        {server_name, "server"},
        {server_root, "/home/javier/"},
        {document_root, "/home/javier/htdocs"},
        {erl_script_alias, {"/api", [server_handler]}},
        {error_log, "error.log"},
        {security_log, "security.log"},
        {transfer_log, "transfer.log"},
        {mime_types, [
            {"html", "text/html"},
            {"css", "text/css"},
            {"js", "application/x-javascript"},
            {"json", "application/json"}
        ]},
        {bind_address, {127,0,0,1}}
    ]).

get_numbers(SessionID, _Env, _Input) -> 
    io:format("Data: ~p~n", [_Input]),
    [{_, Number, _State, _Call}] =  server_table:get_user(_Input),
    %_Str2 = substr(_Input,5,8),
    io:format("Data: ~p~n", [Number]),

    mod_esi:deliver(SessionID,
    ["Content-Type: application/json\r\n\r\n", concat(Number, _State)]).

get_call(SessionID, _Env, _Input) -> 
    io:format("Data: ~p~n", [_Input]),
    [{_, Number, _State, _Call}] =  server_table:get_call(_Input),
    %_Str2 = substr(_Input,5,8),
    io:format("Data: ~p~n", [Number]),
    mod_esi:deliver(SessionID,
    ["Content-Type: application/json\r\n\r\n", concat(Number, _Call)]).

get_all_numbers(SessionID, _Env, _Input) -> 
    io:format("Data: ~p~n", [_Input]),
    Str1 =  server_table:get_all_users(),
    io:format("Data: ~p~n", [Str1]),
     mod_esi:deliver(SessionID,
    ["Content-Type: application/json\r\n\r\n", Str1]).


create_number(SessionID, _Env, _Input)->
    Str1 =  substr(_Input, 1, 4),
    Str2 = substr(_Input,5,8),
    io:format("Data: ~p~n", [Str1]),
    Resp = server_table:insert_number(Str1, Str2),
    case Resp of 
        done ->
        io:format("Data54: ~p~n", [Resp]),
        mod_esi:deliver(SessionID,
        ["Content-Type: application/json\r\n\r\n",Str1])
    end.
    

received_call(SessionID, _Env, _Input)->
    Str1 =  substr(_Input, 1, 4),
    Str2 = substr(_Input,5,8),
    Resp = server_table:update_number(Str1, Str2),
    io:format("Data54: ~p~n", [Resp]),
    mod_esi:deliver(SessionID,
        ["Content-Type: application/json\r\n\r\n",Resp]).

call(SessionID, _Env, _Input)-> 
    Str1 =  substr(_Input, 1, 4),
    Str2 = substr(_Input,5,8),
    Resp = server_table:update_number_call(Str1, Str2),
    mod_esi:deliver(SessionID,
        ["Content-Type: application/json\r\n\r\n",Resp]).

close_all(SessionID, _Env, _Input)->
    Resp = server_table:close_call(_Input),
    mod_esi:deliver(SessionID,
        ["Content-Type: application/json\r\n\r\n",Resp]).