-module(server_table).
-include ("server_records.hrl").
-export([init/0, insert_number/2, get_all_users/0,get_user/1, update_number/2,update_number_call/2,get_call/1,close_call/1]).
-import(string,[concat/2]).
init() -> 
    application:set_env(mnesia,dir,"../db"),
	mnesia:create_schema([node()]),
	io:format("sating from echat ttables~n"),
	mnesia:start(),
    io:format("number table = ~p~n",[mnesia:create_table(cellphone,[{attributes,record_info(fields,cellphone)},{disc_copies,[node()]}])]),
    mnesia:info().

insert_number(Number, State) ->
	User = #cellphone{ number = Number, state =  State, call = ''},
	Fun = fun() ->
				mnesia:write(User)
		  end,
	Trans_result = mnesia:transaction(Fun),
    io:format("result: ~p~n", [Trans_result]),
	case Trans_result of
		{aborted, Reason} ->
			unable_to_insert;
		{atomic, Result} ->
			done;
		_ ->
			unable_to_insert
	end.

get_all_users() ->
	Fun = fun() ->
				mnesia:all_keys(cellphone)
		  end,
	Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    case Lookup of
        {atomic, Cell} ->
	io:format("validity : ~p~n",[Cell]);
        _ -> 
            io:format("Nothing"),
            Cell = []
    end,
    Cell.

get_call(Number) -> 
    Fun = fun() -> 
        mnesia:read(cellphone, Number)
    end,
    Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    case Lookup of
        {atomic, Cell} ->
	io:format("validity : ~p~n",[Cell]);
        _ -> 
            io:format("Nothing"),
            Cell = []
    end,
    Cell.
get_user(Number) ->
    Fun = fun() ->
				mnesia:read(cellphone, Number)
		  end,
	Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    case Lookup of
        {atomic, Cell} ->
	io:format("validity : ~p~n",[Cell]);
        _ -> 
            io:format("Nothing"),
            Cell = []
    end,
    Cell.

update_number(Number, State) -> 
    Fun = fun() ->
        
             [E] = mnesia:read(cellphone, Number),
             New  = E#cellphone{state = State},
             mnesia:write(New)
    end,
    Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    Resp = concat(Number,State),
    Resp.

update_number_call(Number, Call) -> 
    Fun = fun() ->
        
             [E] = mnesia:read(cellphone, Number),
             New  = E#cellphone{call = Call},
             mnesia:write(New)
    end,
    Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    Resp = concat(Number,Call),
    Resp.

close_call(Number) -> 
     Fun = fun() ->
        
             [E] = mnesia:read(cellphone, Number),
             New  = E#cellphone{state = 'true', call = '' },
             mnesia:write(New)
    end,
    Lookup = mnesia:transaction(Fun),
    io:format("validity : ~p~n",[Lookup]),
    Number.