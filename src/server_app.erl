%%%-------------------------------------------------------------------
%% @doc server public API
%% @end
%%%-------------------------------------------------------------------

-module(server_app).

-behaviour(application).

-export([start/2, stop/1]).
-import (server_table, [init/0]).
-import (server_handler, [start/0]).

start(_StartType, _StartArgs) ->
    % Dispatch = cowboy_router:compile([
    %         {'_', 
    %             [
    %            {"/", cowboy_static, {priv_file, server, "index.html"}},
    %             {"/get_all_users", server_handler, []},
    %           {"/assets/[...]",cowboy_static, {priv_dir,server, "assets",[{mimetypes, cow_mimetypes, all}]}}
    %     ]}
    %     ]),
    %     {ok, _} = cowboy:start_clear(http_listener,
    %         [{port, 8090}], 
    %     #{env => #{dispatch => Dispatch}}
    %  ),
    server_table:init(),
    server_handler:start(),
    server_sup:start_link().


stop(_State) ->
    ok.

%% internal functions
